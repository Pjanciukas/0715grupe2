
<html>
    <head>
        <title>YourLifes</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="images/icon.gif" type="image/gif" sizes="16x16">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <!--<h1 id="logo"><a href="index.html">Landed</a></h1>-->
                <nav id="nav">
                    <ul>
						<li><a href="index.php" class="button special">Home</a></li>
                        <li><a href="login.php" class="button special">Login</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Registracija</h2>
                        
					<center>
						<form name="insert_signup" method="get" action="insert.php">
						<table width="274" border="0" align="center" cellpadding="2" cellspacing="0">
  
                            <div class="column uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="fname" value="" placeholder="First Name" required />
                                </div><br>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="text" name="lname" value="" placeholder="Last Name" required />
                                </div><br>
								<div class="6u$ 12u$(xsmall)">
                                    <input type="text" name="username" value="" placeholder="Username" required />
                                </div><br>								
								<div class="6u$ 12u$(xsmall)">
                                    <input type="email" name="email" value="" placeholder="Email" required />
                                </div><br>
								<div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="password" value="" placeholder="Password" required />
                                </div><br>
								<div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Sign Up" name="submit" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
						</center>
                    </header>
                </div>
            </section>
        
<!-- Footer -->
                <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

              </div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
			
		
    </body>
</html>



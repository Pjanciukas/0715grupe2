<!DOCTYPE HTML>
<html>
    <head>
        <title>Prisijungimas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="images/icon.gif" type="image/gif" sizes="16x16">
</head>
<body>
	<div class="container mlogin">
	<div id="login">
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"></h1>
                <nav id="nav">
                    <ul>
						<li><a href="index.php" class="button special">Home</a></li>
                        <li><a href="signup.php" class="button special">Signup</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Prisijungimas</h2>
                        
                        <form method="get" action="prisijungimas.php">
                            <div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="username" placeholder="Username" required  />
                                </div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="password" placeholder="Password" required />
                                </div>
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Login" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </header>
                </div>
            </section>
            
<!-- Footer -->
                <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

            </div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>
<!--<body>
<div class="container mlogin">
<div id="login">
<h3>Prisijungimas</h3>

<form action="" id="loginform" method="post"name="loginform">

<p><label for="user_login">Prisijungimo vardas<br>

<input class="input" id="username" name="username"size="20" type="text" align="center"value=""></label></p>

<p><label>El. paštas<br></label><input name="email" type="text"></p>
	
<p><label for="user_pass">Slaptažodis<br>

<input class="input" id="password" name="password"size="20"type="password" value=""></label></p> 
  
	<p class="submit"><input class="button" name="login"type= "submit" value="Prisijungti"></p>
	
	<p class="regtext">Dar nesate užsiregistravę?<a href= "register.php"> Registracija</a></p>
	
    </form>
	</div>
  </div>
</body>
</html> -->
